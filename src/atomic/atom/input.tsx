import {
    Input as ChakraInput,
    InputProps as ChakraInputProps,
    FormControl,
    FormErrorMessage,
    FormLabel,
} from '@chakra-ui/react'
import { ForwardRefRenderFunction, forwardRef } from 'react'

import { FieldError } from 'react-hook-form'

interface InputProps extends ChakraInputProps {
    name: string
    label?: string
    error?: FieldError
}

const InputBase: ForwardRefRenderFunction<HTMLInputElement, InputProps> = (
    { name, label, error, ...rest },
    ref,
) => {
    return (
        <FormControl isInvalid={!!error}>
            {!!label && (
                <FormLabel
                    htmlFor={name}
                    fontSize={'1.25rem'}
                    fontWeight={'normal'}
                    color={'white'}
                >
                    {label}
                </FormLabel>
            )}

            <ChakraInput
                name={name}
                id={name}
                mb="1"
                w="full"
                rounded="lg"
                p="3"
                size={'lg'}
                color="white"
                _focus={{
                    ring: '2',
                    ringColor: 'primary',
                }}
                ref={ref}
                {...rest}
            />

            {!!error && (
                <FormErrorMessage fontSize={'sm'} color={'red.500'}>
                    {error.message || 'Ops, algo deu errado'}
                </FormErrorMessage>
            )}
        </FormControl>
    )
}

export const Input = forwardRef(InputBase)
