import { Button, ButtonProps } from '@chakra-ui/react'

interface SearchButtonProps extends ButtonProps {
    children: string
}

export function SearchButton({ children, ...rest }: SearchButtonProps) {
    return (
        <Button colorScheme="purple" {...rest} h="50px" w="150px">
            {children}
        </Button>
    )
}
