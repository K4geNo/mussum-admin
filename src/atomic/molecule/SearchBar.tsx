import { Flex } from '@chakra-ui/react'
import { Input } from '../atom/input'
import { SearchButton } from '../atom/SearchButton'
import { useFormContext } from 'react-hook-form'

export function SearchBar() {
    const { register } = useFormContext()

    return (
        <Flex align={'center'} columnGap={'1rem'} pb="4rem">
            <Input
                placeholder="Busque por uma disciplina ou conteúdo..."
                {...register('search')}
            />
            <SearchButton type="submit">Buscar</SearchButton>
        </Flex>
    )
}
