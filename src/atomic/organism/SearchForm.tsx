import { SearchBar } from '../molecule/SearchBar'

export function SearchForm() {
    return (
        <form>
            <SearchBar />
        </form>
    )
}
