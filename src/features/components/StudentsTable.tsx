import {
    Badge,
    Table,
    TableContainer,
    Tbody,
    Td,
    Th,
    Thead,
    Tr,
} from '@chakra-ui/react'

import { StudentsWrapper } from './StudentsWrapper'

export interface Disciplines {
    id: string
    name: string
    students: {
        nodes: {
            id: string
            name: string
        }[]
    }
    lessons: {
        nodes: {
            description: string
            contents: {
                nodes: {
                    description: string
                }[]
            }
        }[]
    }
}

interface StudentsTableProps {
    dataTable: Disciplines[]
}

export function StudentsTable({ dataTable }: StudentsTableProps) {
    return (
        <TableContainer>
            <Table variant={'simple'}>
                <Thead>
                    <Tr>
                        <Th color={'gray.400'}>Disciplina</Th>
                        <Th color={'gray.400'}>Aulas</Th>
                        <Th color={'gray.400'}>Conteúdo</Th>
                        <Th color={'gray.400'}>Alunos</Th>
                    </Tr>
                </Thead>

                <Tbody>
                    {dataTable.map((discipline) => (
                        <Tr key={discipline.id} rounded={'lg'}>
                            <Td>{discipline.name}</Td>
                            <Td>
                                {discipline.lessons.nodes.map((lesson) => (
                                    <Badge key={lesson.description} mr={2}>
                                        {lesson.description}
                                    </Badge>
                                ))}
                            </Td>
                            <Td>
                                {discipline.lessons.nodes.map((lesson) =>
                                    lesson.contents.nodes.map((content) => (
                                        <Badge
                                            key={content.description}
                                            mr={2}
                                            colorScheme="purple"
                                        >
                                            {content.description}
                                        </Badge>
                                    )),
                                )}
                            </Td>
                            <Td>
                                <StudentsWrapper
                                    students={discipline.students}
                                />
                            </Td>
                        </Tr>
                    ))}
                </Tbody>
            </Table>
        </TableContainer>
    )
}
