import {
    Badge,
    Box,
    Button,
    FormLabel,
    HStack,
    Icon,
    Select,
    Stack,
    VStack,
    useToast,
} from '@chakra-ui/react'
import { SubmitHandler, useForm } from 'react-hook-form'
import { useMutation, useQuery } from '@apollo/client'

import { ADD_STUDENT_TO_DISCIPLINE } from '../graphql/mutation/discipline'
import { GET_DISCIPLINES } from '../graphql/query/disciplines'
import { GET_STUDENTS } from '../graphql/query/students'
import { IoIosClose } from 'react-icons/io'
import { useState } from 'react'

type Inputs = {
    student: string
    discipline: string
}

interface StudentsProps {
    students: {
        nodes: {
            id: string
            name: string
            key: string
        }[]
    }
}

interface DisciplineProps {
    disciplines: {
        nodes: {
            id: string
            name: string
        }[]
    }
}

interface Student {
    id: string
    name: string
    key: string
}

export function LinkStudentToDisciplineForm() {
    const [students, setStudents] = useState<Student[]>([])
    const { register, handleSubmit, watch, reset } = useForm<Inputs>()

    const { data } = useQuery<StudentsProps>(GET_STUDENTS)
    const { data: disciplinesData } = useQuery<DisciplineProps>(GET_DISCIPLINES)
    const [addStudentsToDiscipline] = useMutation(ADD_STUDENT_TO_DISCIPLINE)

    const toast = useToast()

    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
            const { discipline } = data

            const filteredStudents = students.map((studentId) => studentId.id)

            await addStudentsToDiscipline({
                variables: {
                    disciplineId: discipline,
                    studentId: filteredStudents,
                },
            })

            reset()

            setStudents([])

            toast({
                title: 'Aluno vinculado com sucesso',
                status: 'success',
                duration: 3000,
                isClosable: true,
                position: 'top-right',
            })
        } catch (error) {
            console.log(error)
            toast({
                title: 'Erro ao vincular aluno',
                status: 'error',
                duration: 3000,
                isClosable: true,
                position: 'top-right',
            })
        }
    }

    function getStudentName(id: string) {
        const student = data?.students.nodes.find(
            (student) => student.id === id,
        )

        return student
    }

    const selectStudentId = watch('student')
    const selectedStudent = getStudentName(selectStudentId)

    const addSelectedStudent = () => {
        if (selectedStudent && !students.includes(selectedStudent)) {
            setStudents([...students, selectedStudent])
        }
    }

    const removeStudent = (studentId: string) => {
        const newStudents = students.filter(
            (student) => student.id !== studentId,
        )

        setStudents(newStudents)
    }

    return (
        <Box as="form" onSubmit={handleSubmit(onSubmit)} pt="1rem">
            <VStack spacing={'1rem'}>
                <Box w="full">
                    <FormLabel fontSize={'1.25rem'} fontWeight={'normal'}>
                        Aluno
                    </FormLabel>

                    <HStack>
                        <Select
                            placeholder="Selecione um aluno"
                            size={'lg'}
                            {...register('student')}
                        >
                            {data?.students.nodes.map((student) => (
                                <option key={student.id} value={student.id}>
                                    {getStudentName(student.id)?.name}
                                </option>
                            ))}
                        </Select>
                        <Button
                            onClick={addSelectedStudent}
                            disabled={
                                !selectedStudent ||
                                students.includes(selectedStudent)
                            }
                        >
                            Adicionar
                        </Button>
                    </HStack>

                    <Stack direction={'row'}>
                        {students.map((student) => (
                            <Badge
                                key={student.id}
                                colorScheme="green"
                                display={'flex'}
                                alignItems={'center'}
                                rounded={'lg'}
                                variant={'outline'}
                                px="10px"
                                py="5px"
                                mt="1rem"
                            >
                                {student.name}

                                <Icon
                                    as={IoIosClose}
                                    ml="0.5rem"
                                    cursor={'pointer'}
                                    onClick={() => removeStudent(student.id)}
                                    boxSize={'22px'}
                                />
                            </Badge>
                        ))}
                    </Stack>
                </Box>

                <Box w="full">
                    <FormLabel fontSize={'1.25rem'} fontWeight={'normal'}>
                        Disciplina
                    </FormLabel>

                    <Select
                        placeholder="Selecione uma disciplina"
                        size={'lg'}
                        {...register('discipline')}
                    >
                        {disciplinesData?.disciplines.nodes.map(
                            (discipline) => (
                                <option
                                    key={discipline.id}
                                    value={discipline.id}
                                >
                                    {discipline.name}
                                </option>
                            ),
                        )}
                    </Select>
                </Box>
            </VStack>

            <Button type="submit" mt="1rem">
                Registrar
            </Button>
        </Box>
    )
}
