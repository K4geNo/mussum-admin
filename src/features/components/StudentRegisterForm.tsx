import { Box, Button, VStack, useToast } from '@chakra-ui/react'
import { SubmitHandler, useForm } from 'react-hook-form'

import { CREATE_STUDENT } from '../graphql/mutation/student'
import { Input } from '@/atomic/atom/input'
import { useMutation } from '@apollo/client'

type Inputs = {
    name: string
    key: string
}

export function StudentRegisterForm() {
    const [create, { loading }] = useMutation(CREATE_STUDENT)
    const { register, handleSubmit, reset } = useForm<Inputs>()

    const toast = useToast()

    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
            await create({
                variables: {
                    name: data.name,
                    key: data.key,
                },
            })

            reset()

            toast({
                title: 'Aluno cadastrado com sucesso',
                status: 'success',
                duration: 3000,
                isClosable: true,
            })
        } catch (error) {
            console.log(error)
            toast({
                title: 'Erro ao cadastrar aluno',
                status: 'error',
                duration: 3000,
                isClosable: true,
            })
        }
    }

    return (
        <Box as="form" onSubmit={handleSubmit(onSubmit)} pt="1rem">
            <VStack spacing={'1rem'}>
                <Input
                    label="Nome"
                    placeholder="Nome do aluno"
                    {...register('name')}
                />

                <Input
                    label="Matrícula"
                    placeholder="Matrícula do aluno"
                    {...register('key')}
                />
            </VStack>

            <Button type="submit" mt="1rem" isLoading={loading}>
                Registrar
            </Button>
        </Box>
    )
}
