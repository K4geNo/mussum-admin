import {
    Box,
    Button,
    FormLabel,
    Select,
    VStack,
    useToast,
} from '@chakra-ui/react'
import { SubmitHandler, useForm } from 'react-hook-form'
import { useMutation, useQuery } from '@apollo/client'

import { ADD_LESSON_TO_DISCIPLINE } from '../graphql/mutation/discipline'
import { GET_DISCIPLINES } from '../graphql/query/disciplines'
import { GET_LESSONS } from '../graphql/query/lesson'

type Inputs = {
    lesson: string
    discipline: string
}

interface LessonProps {
    lessons: {
        nodes: {
            id: string
            description: string
        }[]
    }
}

interface DisciplineProps {
    disciplines: {
        nodes: {
            id: string
            name: string
        }[]
    }
}

export function LinkDisciplineToLessonForm() {
    const { register, handleSubmit, reset } = useForm<Inputs>()

    const { data: lessonsData } = useQuery<LessonProps>(GET_LESSONS)
    const { data: disciplinesData } = useQuery<DisciplineProps>(GET_DISCIPLINES)
    const [addLessonsToDiscipline] = useMutation(ADD_LESSON_TO_DISCIPLINE)

    const toast = useToast()

    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
            const { lesson, discipline } = data

            await addLessonsToDiscipline({
                variables: {
                    disciplineId: discipline,
                    lessonId: lesson,
                },
            })

            toast({
                title: 'Aula vinculada com sucesso',
                status: 'success',
                duration: 3000,
                isClosable: true,
                position: 'top-right',
            })

            reset()
        } catch (error) {
            console.log(error)
            toast({
                title: 'Erro ao vincular aula',
                status: 'error',
                duration: 3000,
                isClosable: true,
                position: 'top-right',
            })
        }
    }

    return (
        <Box as="form" onSubmit={handleSubmit(onSubmit)} pt="1rem">
            <VStack spacing={'1rem'}>
                <Box w="full">
                    <FormLabel fontSize={'1.25rem'} fontWeight={'normal'}>
                        Disciplina
                    </FormLabel>

                    <Select
                        placeholder="Selecione uma disciplina"
                        size={'lg'}
                        {...register('discipline')}
                    >
                        {disciplinesData?.disciplines.nodes.map(
                            (discipline) => (
                                <option
                                    key={discipline.id}
                                    value={discipline.id}
                                >
                                    {discipline.name}
                                </option>
                            ),
                        )}
                    </Select>
                </Box>

                <Box w="full">
                    <FormLabel fontSize={'1.25rem'} fontWeight={'normal'}>
                        Aulas
                    </FormLabel>

                    <Select
                        placeholder="Selecione a quantidade de aulas"
                        size={'lg'}
                        {...register('lesson')}
                    >
                        {lessonsData?.lessons.nodes.map((lesson) => (
                            <option key={lesson.id} value={lesson.id}>
                                {lesson.description}
                            </option>
                        ))}
                    </Select>
                </Box>
            </VStack>

            <Button type="submit" mt="1rem">
                Registrar
            </Button>
        </Box>
    )
}
