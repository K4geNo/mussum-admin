'use client'

import {
    Box,
    Button,
    FormLabel,
    Select,
    VStack,
    useToast,
} from '@chakra-ui/react'
import { SubmitHandler, useForm } from 'react-hook-form'
import { useMutation, useQuery } from '@apollo/client'

import { CREATE_CONTENT } from '../graphql/mutation/contents'
import { GET_LESSONS } from '../graphql/query/lesson'
import { Input } from '@/atomic/atom/input'

type Inputs = {
    content: string
    lesson: string
}

interface LessonProps {
    lessons: {
        nodes: {
            id: string
            description: string
        }[]
    }
}

export function ContentRegisterForm() {
    const { data: lessonsData } = useQuery<LessonProps>(GET_LESSONS)

    const [createOneContent] = useMutation(CREATE_CONTENT)
    const { register, handleSubmit, reset } = useForm<Inputs>()

    const toast = useToast()

    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
            await createOneContent({
                variables: {
                    description: data.content,
                    lessonId: data.lesson,
                },
            })

            toast({
                title: 'Conteúdo registrado com sucesso',
                status: 'success',
                duration: 3000,
                isClosable: true,
                position: 'top-right',
            })

            reset()
        } catch (error) {
            console.log(error)
            toast({
                title: 'Erro ao registrar conteúdo',
                status: 'error',
                duration: 3000,
                isClosable: true,
                position: 'top-right',
            })
        }
    }

    return (
        <Box as="form" onSubmit={handleSubmit(onSubmit)} pt="1rem">
            <VStack spacing={'1rem'}>
                <Input
                    label="Conteúdo"
                    placeholder="Nome do conteúdo"
                    {...register('content')}
                />

                <Box w="full">
                    <FormLabel fontSize={'1.25rem'} fontWeight={'normal'}>
                        Aula
                    </FormLabel>

                    <Select
                        placeholder="Seleciona a aula que o conteúdo pertence"
                        size={'lg'}
                        {...register('lesson')}
                    >
                        {lessonsData?.lessons.nodes.map((lesson) => (
                            <option key={lesson.id} value={lesson.id}>
                                {lesson.description}
                            </option>
                        ))}
                    </Select>
                </Box>
            </VStack>

            <Button type="submit" mt="1rem">
                Registrar
            </Button>
        </Box>
    )
}
