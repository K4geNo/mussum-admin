import { Box, Container, Heading, Link, Stack } from '@chakra-ui/react'

import NextLink from 'next/link'

export function Header() {
    return (
        <Box
            as="header"
            py="24px"
            bg="gray.900"
            boxShadow={'0 0 10px rgba(0, 0, 0, 0.2)'}
        >
            <Container
                maxW="8xl"
                display="flex"
                justifyContent="space-between"
                alignItems="center"
            >
                <Heading fontSize={'3xl'} fontStyle={'italic'}>
                    MussumAdmin
                </Heading>

                <Stack as="nav" spacing={'2rem'} direction="row">
                    <Link
                        as={NextLink}
                        href="/"
                        _hover={{
                            textDecoration: 'none',
                            color: 'purple.400',
                        }}
                    >
                        Home
                    </Link>
                    <Link
                        as={NextLink}
                        href="/registrar-aluno"
                        _hover={{
                            textDecoration: 'none',
                            color: 'purple.400',
                        }}
                    >
                        Registrar aluno
                    </Link>
                </Stack>
            </Container>
        </Box>
    )
}
