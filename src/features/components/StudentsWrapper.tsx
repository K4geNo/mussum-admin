import {
    Button,
    Drawer,
    DrawerBody,
    DrawerCloseButton,
    DrawerContent,
    DrawerHeader,
    DrawerOverlay,
    Flex,
    Heading,
    Icon,
    useDisclosure,
} from '@chakra-ui/react'
import { useMutation, useQuery } from '@apollo/client'

import { BiTrash } from 'react-icons/bi'
import { DELETE_STUDENT } from '../graphql/mutation/student'
import { Disciplines } from './StudentsTable'
import { GET_DISCIPLINES } from '../graphql/query/disciplines'

interface StudentsWrapperProps {
    students: {
        nodes: {
            id: string
            name: string
        }[]
    }
}

export function StudentsWrapper({ students }: StudentsWrapperProps) {
    const { isOpen, onOpen, onClose } = useDisclosure()

    const [DeleteStudent] = useMutation(DELETE_STUDENT)

    const { refetch } = useQuery(GET_DISCIPLINES)

    async function handleDeleteStudent(id: string) {
        await DeleteStudent({
            variables: {
                id,
            },
        })

        await refetch()
    }

    return (
        <>
            <Button colorScheme="purple" onClick={onOpen} py="12px" px="16px">
                Ver alunos
            </Button>
            <Drawer isOpen={isOpen} placement="right" onClose={onClose}>
                <DrawerOverlay />
                <DrawerContent bg="gray.800">
                    <DrawerCloseButton />
                    <DrawerHeader
                        pt="50px"
                        borderBottom={'1px solid #eaeaea'}
                        fontWeight={'normal'}
                    >
                        Alunos registrados no curso
                    </DrawerHeader>

                    <DrawerBody>
                        {students.nodes.map((student, index) => (
                            <Flex
                                align={'center'}
                                justifyContent={'space-between'}
                                key={student.id}
                                pt={'2rem'}
                            >
                                <Heading size="md" fontWeight={'normal'}>
                                    {index + 1} - {student.name}
                                </Heading>

                                <Icon
                                    as={BiTrash}
                                    cursor={'pointer'}
                                    boxSize={'22px'}
                                    onClick={() =>
                                        handleDeleteStudent(student.id)
                                    }
                                />
                            </Flex>
                        ))}
                    </DrawerBody>
                </DrawerContent>
            </Drawer>
        </>
    )
}
