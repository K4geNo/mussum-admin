import { Tab, TabList, TabPanel, TabPanels, Tabs } from '@chakra-ui/react'

import { ContentRegisterForm } from './ContentRegisterForm'
import { DisciplineRegisterForm } from './DisciplineRegisterForm'
import { LinkDisciplineToLessonForm } from './LinkDisciplineToLessonForm'
import { LinkStudentToDisciplineForm } from './LinkStudentToDisciplineForm'
import { StudentRegisterForm } from './StudentRegisterForm'

const tabHeader = [
    {
        label: 'Cadastrar aluno',
    },
    {
        label: 'Cadastrar disciplina',
    },
    {
        label: 'Cadastrar conteúdo',
    },
    {
        label: 'Vincular disciplina',
    },
    {
        label: 'Vincular aluno',
    },
]

export function DataTables() {
    return (
        <Tabs isLazy variant={'soft-rounded'} colorScheme="purple">
            <TabList>
                {tabHeader.map((tab, index) => (
                    <Tab key={index} textTransform={'uppercase'}>
                        {tab.label}
                    </Tab>
                ))}
            </TabList>
            <TabPanels>
                <TabPanel>
                    <StudentRegisterForm />
                </TabPanel>
                <TabPanel>
                    <DisciplineRegisterForm />
                </TabPanel>
                <TabPanel>
                    <ContentRegisterForm />
                </TabPanel>
                <TabPanel>
                    <LinkDisciplineToLessonForm />
                </TabPanel>
                <TabPanel>
                    <LinkStudentToDisciplineForm />
                </TabPanel>
            </TabPanels>
        </Tabs>
    )
}
