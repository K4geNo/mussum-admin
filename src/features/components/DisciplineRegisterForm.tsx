import { Box, Button, VStack, useToast } from '@chakra-ui/react'
import { SubmitHandler, useForm } from 'react-hook-form'

import { CREATE_DISCIPLINE } from '../graphql/mutation/discipline'
import { CREATE_LESSON } from '../graphql/mutation/lesson'
import { Input } from '@/atomic/atom/input'
import { useMutation } from '@apollo/client'

type Inputs = {
    discipline: string
    lesson: string
}

export function DisciplineRegisterForm() {
    const [createDiscipline, { loading }] = useMutation(CREATE_DISCIPLINE)

    const [createLesson] = useMutation(CREATE_LESSON)
    const { register, handleSubmit, reset } = useForm<Inputs>()

    const toast = useToast()

    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
            await createDiscipline({
                variables: {
                    name: data.discipline,
                },
            })

            await createLesson({
                variables: {
                    description: data.lesson,
                },
            })

            toast({
                title: 'Disciplina registrada com sucesso',
                status: 'success',
                duration: 3000,
                isClosable: true,
                position: 'top-right',
            })

            reset()
        } catch (error) {
            console.log(error)
            toast({
                title: 'Erro ao registrar disciplina',
                status: 'error',
                duration: 3000,
                isClosable: true,
                position: 'top-right',
            })
        }
    }

    return (
        <Box as="form" onSubmit={handleSubmit(onSubmit)} pt="1rem">
            <VStack spacing={'1rem'}>
                <Input
                    label="Disciplina"
                    placeholder="Nome da disciplina"
                    {...register('discipline')}
                />

                <Input
                    label="Aula"
                    placeholder="Nome da aula"
                    {...register('lesson')}
                />
            </VStack>

            <Button type="submit" mt="1rem" isLoading={loading}>
                Registrar
            </Button>
        </Box>
    )
}
