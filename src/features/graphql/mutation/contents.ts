import { gql } from '@apollo/client'

export const CREATE_CONTENT = gql`
    mutation createOneContent($description: String!, $lessonId: String!) {
        createOneContent(
            input: {
                content: { description: $description, lessonId: $lessonId }
            }
        ) {
            id
            description
        }
    }
`
