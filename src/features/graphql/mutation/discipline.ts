import { gql } from '@apollo/client'

export const CREATE_DISCIPLINE = gql`
    mutation createOneDiscipline($name: String!) {
        createOneDiscipline(input: { discipline: { name: $name } }) {
            id
            name
            createdAt
        }
    }
`

export const ADD_STUDENT_TO_DISCIPLINE = gql`
    mutation addStudentsToDiscipline($disciplineId: ID!, $studentId: [ID!]!) {
        addStudentsToDiscipline(
            input: { id: $disciplineId, relationIds: $studentId }
        ) {
            id
            students {
                nodes {
                    name
                }
            }
        }
    }
`

export const ADD_LESSON_TO_DISCIPLINE = gql`
    mutation addLessonsToDiscipline($disciplineId: ID!, $lessonId: [ID!]!) {
        addLessonsToDiscipline(
            input: { id: $disciplineId, relationIds: $lessonId }
        ) {
            id
            students {
                nodes {
                    name
                }
            }
        }
    }
`
