import { gql } from '@apollo/client'

export const CREATE_LESSON = gql`
    mutation createOneLesson($description: String!) {
        createOneLesson(input: { lesson: { description: $description } }) {
            id
            description
            createdAt
        }
    }
`
