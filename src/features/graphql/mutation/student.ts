import { gql } from '@apollo/client'

export const CREATE_STUDENT = gql`
    mutation CreateStudent($key: String!, $name: String!) {
        createOneStudent(input: { student: { key: $key, name: $name } }) {
            id
            key
            name
        }
    }
`

export const DELETE_STUDENT = gql`
    mutation DeleteStudent($id: ID!) {
        deleteOneStudent(input: { id: $id }) {
            id
            name
        }
    }
`
