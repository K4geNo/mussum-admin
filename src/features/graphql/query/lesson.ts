import { gql } from '@apollo/client'

export const GET_LESSONS = gql`
    query GetLessons {
        lessons {
            nodes {
                id
                description
            }
            totalCount
        }
    }
`
