import { gql } from '@apollo/client'

export const GET_ALL_DATA_DISCIPLINES = gql`
    query GetAllDataDisciplines {
        disciplines {
            nodes {
                id
                name
                students {
                    nodes {
                        id
                        name
                    }
                }
                lessons {
                    nodes {
                        description
                        contents {
                            nodes {
                                description
                            }
                        }
                    }
                }
            }
            totalCount
        }
    }
`

export const GET_DISCIPLINES = gql`
    query GetDisciplines {
        disciplines {
            nodes {
                id
                name
            }
        }
    }
`
