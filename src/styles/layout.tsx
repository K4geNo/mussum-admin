import { Box, Container } from '@chakra-ui/react'

import { Header } from '@/features/components/Header'

export function Layout({ children }: { children: React.ReactNode }) {
    return (
        <Box w="full" minH={'100vh'}>
            <Header />
            <Container maxW="8xl">{children}</Container>
        </Box>
    )
}
