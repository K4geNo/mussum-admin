'use client'

import { DataTables } from '@/features/components/DataTables'
import { Flex } from '@chakra-ui/react'

export default function RegistarAluno() {
    return (
        <Flex flexDir={'column'} py="8rem">
            <DataTables />
        </Flex>
    )
}
