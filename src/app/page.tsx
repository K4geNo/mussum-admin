'use client'

import { Disciplines, StudentsTable } from '@/features/components/StudentsTable'
import { Flex, Heading } from '@chakra-ui/react'
import { FormProvider, useForm } from 'react-hook-form'

import { GET_ALL_DATA_DISCIPLINES } from '@/features/graphql/query/disciplines'
import { SearchBar } from '@/atomic/molecule/SearchBar'
import { useQuery } from '@apollo/client'
import { useState } from 'react'

type Input = {
    search: string
}

export default function Home() {
    const [discipline, setDiscipline] = useState<Disciplines[]>([])
    const { data: disciplinesData, loading } = useQuery(
        GET_ALL_DATA_DISCIPLINES,
    )

    const methods = useForm<Input>()

    const onSubmit = async (data: Input) => {
        const filteredDisciplines = disciplinesData.disciplines.nodes.filter(
            (discipline: { name: string }) => {
                return discipline.name
                    .toLowerCase()
                    .includes(data.search.toLowerCase())
            },
        )

        setDiscipline(filteredDisciplines)
    }

    return (
        <Flex flexDir={'column'} h="full" py="8rem">
            <FormProvider {...methods}>
                <form onSubmit={methods.handleSubmit(onSubmit)}>
                    <SearchBar />
                </form>
            </FormProvider>

            {loading && <Heading>Carregando...</Heading>}

            {!loading && disciplinesData.disciplines.nodes.length > 0 && (
                <StudentsTable
                    dataTable={
                        discipline.length > 0
                            ? discipline
                            : disciplinesData.disciplines.nodes
                    }
                />
            )}

            {!loading && disciplinesData.disciplines.nodes.length === 0 && (
                <Heading>Não há disciplinas cadastradas</Heading>
            )}
        </Flex>
    )
}
